import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { CrudService } from './../../service/crud.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  storage = {
    type: 'Local storage',
    option: [
      'localStorage', 'cache', 'jsonServer'
    ]
  }

  modalRef?: BsModalRef;

  @ViewChild('cacheModal') cacheModal!: any

  constructor(
    private crudService: CrudService,
    private router:Router,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.crudService.setStorage({storageType: 'localStorage'})
  }

  setStorage(type:any){
    if (type && type.length > 0) {
      switch (type) {
        case 'localStorage':
          this.storage.type = 'Local storage'
          break;
        case 'cache':
          // this.storage.type = 'Client-side cache'
          type = 'localStorage'
          this.storage.type = 'Local storage'
          this.openModal(this.cacheModal)
          break;
        case 'jsonServer':
          this.storage.type = 'JSON server'
          break;
      
        default:
          break;
      }

      this.crudService.setStorage({storageType: type})
      this.router.navigate(['/dashboard']);

    }
  }
  
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

}
