import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CrudService } from './../service/crud.service'

@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.scss']
})
export class RoutingComponent implements OnInit {

  routeList:any = []

  saved = false
  timer = 3

  routing$ = this.crudService.routing$

  constructor(
    public router: Router,
    private crudService: CrudService  ) { }

  ngOnInit(): void {
    /* let routeList = localStorage.getItem('routing')
    if (routeList) {
      this.routeList = JSON.parse(routeList)
    } else {
      let save = [{route:"one"},{route:"two"},{route:"three"}]
      this.routeList = save
      localStorage.setItem('routing', JSON.stringify(save))
    } */
    this.routing$.subscribe(res=>{
      this.routeList = res
    })
  }

  updateRouting(data: any) {
    console.log(data);
    
    this.saved = true
    if (data.id){
      this.crudService.updateRouting(data)
    }
    setTimeout(() => {
      this.router.navigate(['/dashboard'])
    }, 3000);
    setInterval(()=>{
      this.timer = this.timer - 1
    },1000)
  }

}
