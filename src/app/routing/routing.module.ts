import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { RoutingRoutingModule } from './routing-routing.module'

import { RoutingComponent } from './routing.component';



@NgModule({
  declarations: [
    RoutingComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RoutingRoutingModule
  ]
})
export class RoutingModule { }
