import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JsonServerService {

  public jsonServerUrl = 'http://localhost:3000' 

  constructor(
    private http: HttpClient
  ) { }

  getEmployees() {
    return this.http.get(this.jsonServerUrl+`/employees`)
  }

  getEmployeeById(obj:any){
    return this.http.get(this.jsonServerUrl+`/employees/${obj.id}`)
  }

  updateEmployee(obj:any){
    console.log(obj);
    return this.http.put(this.jsonServerUrl+`/employees/${obj.id}`, { ...obj })
  }

  addEmployee(obj:any){
    console.log(obj);
    return this.http.post(this.jsonServerUrl+`/employees`, { ...obj })
  }

  getRouting() {
    return this.http.get(this.jsonServerUrl+`/routing`)
  }

  updateRouting(obj:any){
    console.log(obj);
    return this.http.put(this.jsonServerUrl+`/routing/${obj.id}`, { ...obj })
  }
}
