import { Injectable } from '@angular/core';
import { BehaviorSubject, lastValueFrom } from 'rxjs';

import { LocalStorageService } from './local-storage.service'
import { JsonServerService } from './json-server.service'
@Injectable({
  providedIn: 'root'
})
export class CrudService {

  private storageSelected: BehaviorSubject<any> = new BehaviorSubject({})
  public storageSelected$ = this.storageSelected.asObservable()

  private employees: BehaviorSubject<any> = new BehaviorSubject([])
  public employees$ = this.employees.asObservable()

  private routing: BehaviorSubject<any> = new BehaviorSubject([])
  public routing$ = this.routing.asObservable()

  private employee: BehaviorSubject<any> = new BehaviorSubject({})
  public employee$ = this.employee.asObservable()

  storageType = 'jsonServer'
  storageOption = ['localStorage', 'cache', 'jsonServer']

  constructor(
    private localStorageService: LocalStorageService,
    private jsonServerService: JsonServerService
  ) { }

  setStorage(type:any){
    console.log(type);
    
    this.storageType = type.storageType
    this.storageSelected.next({...type})
  }

  async getEmployees(){
    let data: any
    if (this.storageType == 'localStorage') { 
       data = await this.localStorageService.getEmployees();    
    }
    
    if (this.storageType == 'jsonServer') { 
      data = await lastValueFrom(this.jsonServerService.getEmployees()) 
    }
    
    this.employees.next([...data])
    this.employee.next({})
    return data
  }

  async getEmployeeById(obj:any){
    let data: any
    if (this.storageType == 'localStorage') { 
      data = this.localStorageService.getEmployeeById(obj);    
    }
    
    if (this.storageType == 'jsonServer') { 
      data = await lastValueFrom(this.jsonServerService.getEmployeeById(obj)) 
    }
    
    this.employee.next({...data})
    return data
  }

  async updateEmployee(value:any){
    let data: any
    if (this.storageType == 'localStorage') { 
      data = this.localStorageService.updateEmployee(value);    
    }
    
    if (this.storageType == 'jsonServer') { 
      data = await lastValueFrom(this.jsonServerService.updateEmployee(value)) 
    }
    this.employee.next({...data})
    return data
  }



  async addEmployee(value:any){
    let data: any
    
    if (this.storageType == 'localStorage') { 
      data = this.localStorageService.addEmployee(value)
    }
    
    if (this.storageType == 'jsonServer') { 
      data = await lastValueFrom(this.jsonServerService.addEmployee(value)) 
    }
    // this.employees.next([this])
    return data
    
  }

  async getRouting(){
    console.log(this.storageType);
    let data: any
    if (this.storageType == 'localStorage') { 
       data = await this.localStorageService.getRouting();    
    }
    
    if (this.storageType == 'jsonServer') { 
      data = await lastValueFrom(this.jsonServerService.getRouting()) 
    }
    
    this.routing.next([...data])
    return data
  }

  async updateRouting(value:any){
    let data: any
    if (this.storageType == 'localStorage') { 
      data = this.localStorageService.updateRouting(value);    
    }
    
    if (this.storageType == 'jsonServer') { 
      data = await lastValueFrom(this.jsonServerService.updateRouting(value)) 
    }
    console.log(data);
    
    this.routing.next([...data])
    return data
  }

}
