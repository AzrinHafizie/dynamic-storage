import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  getEmployees() {
    let employees:any = localStorage.getItem('employees')

    if (employees) {
      employees = JSON.parse(employees)
    } else {
      let obj = { 
        id: 1,
        name: 'Amy Star',
        gender: 'female',
        email: 'amy@star.com',
        phone: '+60146667667',
        nationality: 'Malaysia',
        username: 'amy',
      }

      let arr = JSON.stringify([{...obj}])
      localStorage.setItem('employees', arr)
      employees = [obj]
    }
    

    return employees ? employees : [];
  }

  getEmployeeById(obj:any){
    let employees = <any>localStorage.getItem('employees')
    let employee = {}
    if(employees && employees.length>0) {
      employees = JSON.parse(employees)
      let data = employees!.filter((f:any)=>f.id==obj.id)
      employee = data[0]
    }
    return employee
  }

  updateEmployee(obj:any){
    let employees = <any>localStorage.getItem('employees')
    let employee = {}
    if(employees && employees.length>0) {
      employees = JSON.parse(employees)
      let data = employees!.map((m:any)=>{
        if(m.id==obj.id) {
          m = {...obj}
          employee = m
        }
        return m
      })
      employees = JSON.stringify([...data])
      localStorage.setItem('employees', employees)
    }
    return employee
  }

  addEmployee(obj:any){
    let employees = <any>localStorage.getItem('employees')
    let employee = {}
    if(employees && employees.length>0) {
      employees = JSON.parse(employees)
      let ids = employees!.map((m:any)=>{
        return m.id
      })
      console.log(ids);
      let maxId = Math.max(ids)
      let person = {...obj, id:maxId+1}
      employees = JSON.stringify([...employees, person])
      localStorage.setItem('employees', employees)
    }
    return employee
  }

  getRouting() {
    let routing:any = localStorage.getItem('routing')
    if (routing) {
      routing = JSON.parse(routing)
    } else {
      let arr:any = [ 
        { 
          id: 1,
          name: 'One',
          route: 'one',
        },
        {
          id: 2,
          name: 'Two',
          route: 'two'
        },
        {
          id: 3,
          name: 'Three',
          route: 'three'
        }
      ]

      localStorage.setItem('routing', JSON.stringify([...arr]))
      routing = arr
    }
    
    return routing ? routing : [];
  }

  updateRouting(obj:any){
    let routing = <any>localStorage.getItem('routing')
    let route = {}
    if(routing && routing.length>0) {
      routing = JSON.parse(routing)
      let data = routing!.map((m:any)=>{
        if(m.id==obj.id) {
          m = {...obj}
          route = m
        }
        return m
      })
      routing = JSON.stringify([...data])
      localStorage.setItem('routing', routing)
    }
    return JSON.parse(routing)
  }

}
