import { Component, OnInit } from '@angular/core';

import { CrudService } from './../service/crud.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  storageSelected$ = this.crudService.storageSelected$
  employees$ = this.crudService.employees$
  routing$ = this.crudService.routing$

  constructor(
    private crudService: CrudService
  ) { }

  ngOnInit(): void {
    this.storageSelected$.subscribe(res=>{
      this.crudService.getEmployees()
      this.crudService.getRouting()
    })
    /* this.employees$.subscribe(res => {
      console.log(res);
    }) */
    /* this.routing$.subscribe(res => {
      console.log(res);
    }) */
    
  }

  

}
