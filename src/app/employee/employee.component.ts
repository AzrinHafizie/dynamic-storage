import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService } from './../service/crud.service'


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  employee$ = this.crudService.employee$
  employee:any = {}

  public employeeForm!: FormGroup;

  saved = false
  timer = 3

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private crudService: CrudService
  ) { }

  ngOnInit(): void {
    this.employeeForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required],
      gender: [''],
      email: [''],
      nationality: ['']
    });

    this.route.queryParams.subscribe((res:any)=>{
      if (res && res.employeeId) {
        this.crudService.getEmployeeById({id: res.employeeId})
      }
    })

    this.employee$.subscribe(res=>{
      this.employee = res
      this.employeeForm.patchValue({
        id: res.id,
        name: res.name,
        gender: res.gender,
        email: res.email,
        nationality: res.nationality,
      })
      
    })

  }

  updateEmployee(){
    this.saved = true
    if (this.employeeForm.valid){
      let data = {...this.employee, ...this.employeeForm.value}
      this.crudService.updateEmployee(data)
    } else {
      this.crudService.addEmployee(this.employeeForm.value)
    }
    setTimeout(() => {
      this.router.navigate(['/dashboard'])
    }, 3000);
    setInterval(()=>{
      this.timer = this.timer - 1
    },1000)
  }

}
