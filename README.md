## First Setup

Run following command in terminal / command prompt.
```
npm i
```

## Start on mac

Run following command in terminal.
```
npm run start
```

## Start on windows

Run following command in terminal.
```
npm run startwindows
```